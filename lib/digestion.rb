module Digestion
  def digest(data)
    encode_string = data.unescape
    Base64.encode64 OpenSSL::HMAC.digest('sha256',ApiConfig.privateKey, encode_string)
  end

  def secret_key
    ApiConfig.privateKey
  end
end
