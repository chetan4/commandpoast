class Store
  attr_reader :attributes
  def initialize(attributes)
    @attributes = serialize_attributes(attributes)
  end

  def valid?
    attributes['access_token_expires_at'].to_i > Time.now.utc.to_i
  end

  private
  def serialize_attributes(attributes)
    JSON.parse(attributes)
  end

  class << self
    def find(token)
      return if connection_closed? or RedisConnection.get(token).nil?
      store = new(RedisConnection.get(token))
      store.attributes if store.valid?
    end

    def put(key,value)
      return if connection_closed?
      RedisConnection.set(key,value)
    end

    def del(token)
      return if connection_closed?
      RedisConnection.del(token)
    end

    def connection_open?
      connected?
    end

    def connection_closed?
      not(connected?)
    end

    private
    def connected?
      RedisConnection.ping
    rescue Redis::CannotConnectError
      return nil
    end
  end
end
