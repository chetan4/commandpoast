class DecodeError < StandardError; end
class InvalidDigest < StandardError; end

class Parser
  def self.attributes
    @attributes
  end

  def self.parse(response)
    @attributes = response
    if attributes[:encode] or attributes[:digest]
      ## Do symmetric check
      valid_digestion rescue invaild_digestion!
    end

    if attributes[:encode]
      decodify! rescue fail_decoding!
    end
    attributes
  end

  class << self
    def valid_digestion
      begin
        digest_string = CGI.unescape(attributes.delete(:digest))
        (digest_string == session_digest) ? true : 0/0 ##  will throw an error
      rescue
        raise InvalidDigest
      end
    end

    def session_digest
      encode_string = (attributes[:encode] || attributes['encode'])
      unescaped_encode_string = CGI.unescape(encode_string)
      ApiUtil::Digest.generate_digest(unescaped_encode_string, ApiConfig.privateKey)
    end


    def invalid_digestion!
      reset_attributes
      @attributes = {request_status: :failed,error_message: 'Failed Parsing since response is invalid'}
    end

    def fail_decoding!
      reset_attributes
      @attributes = {request_status: :failed,error_message: 'Fail to Decode the params'}
    end

    def decodify!
      decoded_data = ApiUtil::Code.decode(attributes.delete(:encode).unescape)
      decoded_output = ApiUtil::Code.decode_json(decoded_data)
      raise DecodeError unless decoded_output.is_a?(Hash)
      attributes.merge!(decoded_output)
    end

    def reset_attributes
      @attributes = {}
    end
  end
end


class Response < Parser
  def initialize(attributes)
    @data = Parser.parse(attributes)
    @status = data.delete(:request_status)
    @errors = data.delete(:error_message)
    @error
  end

  def failed?
    status.to_sym == :failed or status.to_sym == :failure
  end

  def success?
    status.to_sym == :success
  end

  def status
    @status.to_sym rescue :failed
  end

  def errors
    [@errors].compact.flatten
  end

  def data
    @data
  end
end
