class ServiceObject
  include HTTParty
  extend Digestion
  base_uri ApiConfig.signon_url
  class << self
    def fetch_info(options)
      @params = options
      parse(make_request)
    end

    def create_info(options)
      @params = options
      parse(make_request)
    end

    def update_info(options)
      @params = options
      parse(make_request)
    end

    def remove_info(options)
      @params = options
      parse(make_request)
    end

    private
    def make_request
      response = case params.delete(:method)
      when :post
        make_post_request
      when :put
        make_put_request
      when :delete
        make_delete_request
      else
        make_get_request
      end
      response.body
    end

    def make_get_request
      get(params.delete(:path),:query => query_params)
    end

    def make_post_request
      post(params.delete(:path),:query => query_params)
    end

    def make_put_request
      put(params.delete(:path),query: query_params)
    end

    def make_delete_request
      delete(params.delete(:path),query: query_params)
    end

    def params
      @params
    end

    def query_params
      query_params = {}

      if params.any? and not params[:plain]
        encode_string = ApiUtil::Code.encode(params.to_json)
        digest = ApiUtil::Digest.generate_digest(encode_string, ApiConfig.privateKey)
        escaped_encode_string = CGI.escape(encode_string)
        escaped_digest = CGI.escape(digest)
        query_params[:encode] = escaped_encode_string
        query_params[:digest] = escaped_digest
      end

      if params.delete(:plain)
        query_params.merge!(params)
      end

      query_params[:api_key] = ApiConfig.apiKey
      ## It a local variable not an invocation to method itself
      query_params
    end

    def parse(raw_data)
      Rails.logger.info "Raw Data : - #{raw_data}"
      attributes = HashWithIndifferentAccess.new(to_hash(raw_data))
      build_response(attributes)
    end

    def to_hash(data)
      JSON.parse(data)
    end

    def symmetric_check?(attributes)
      digest_string = CGI.unescape(attributes.delete(:digest))
      digest_string == session_digest(attributes) rescue nil
    end

    def session_digest(attributes)
      encode_string = (attributes[:encode] || attributes['encode'])
      unescaped_encode_string = CGI.unescape(encode_string)
      ApiUtil::Digest.generate_digest(unescaped_encode_string, ApiConfig.privateKey)
    end

    def encode(data)
      data.to_json.encode
      #Base64.encode64 data.to_json
    end

    def decode(data)
      return data unless data.instance_of?(String)
      decoded_data = ApiUtil::Code.decode(data)
      ApiUtil::Code.decode_json(decoded_data)
    end

    def build_response(attributes)
      Response.new(attributes)
    end
  end
end
