require 'csv'
class CSVParser
  attr_reader :file,:parse_options,:csv_rows
  def initialize(file)
    @file = file
    @parse_options =  {encoding: 'utf-8',col_sep:',',skip_blanks: true,headers: true,force_quotes: true}
    @csv_rows = read
  end

  def read
    # CSV.read(file,parse_options)
    quote_chars = %w(" | ~ ^ & *)
    begin
      @report = CSV.read(file, headers: :first_row, quote_char: quote_chars.shift,encoding: 'utf-8',col_sep:',')
    rescue CSV::MalformedCSVError
      quote_chars.empty? ? raise : retry 
    end
  end

  def to_hash
    csv_rows.map do |row|
      store(row)
    end
  end

  # def companies
  #   @companies = User.instance_variables_get("@companies")
  # end

  def headers
    csv_rows.headers
  end

  def store(row)
    attributes = {}
    headers.each_with_index do |header,index|
      attributes[header.strip.gsub("'", '').to_sym.downcase] = row[index]
    end
    custom_headers(attributes)
  end
  ##
  def custom_headers(attributes)
    attributes
  end

  def validate_rows
    count_row = 0
    #@passed_file is a csv file which is read from a submitted html form.
    begin
      @parsed_file = CSV.parse(file)
      @parsed_file.each do |row|
        # Array of columns to be validated
        validate_cols = ['first_name', 'last_name', 'email', 'company', 'roles']
        rowdata = {'first_name' => row[0], 'last_name' => row[1], 'email' => row[2], 'company' => row[3], 'roles' => row[4]}

        #validate colums by sending value from the array (rowdata)  to a method injected
        #by the following code. However value from rowdata array is not passed
        #to the method.
        if count_row == 0
          valid = validate_cols.inject(true){|valid_sum, col|
              valid_sum && send("validate_#{col}", rowdata[col])
          }
          #if validation fails return row number.
          if not (valid)
            msg = "Incorrect columns. Columns should be in this order: #{validate_cols.join(", ")}"
            return false, msg
          end
        end
        break
        # count_row = count_row + 1
      end 
      #if validation suceeds return 0
      return 0
    rescue CSV::MalformedCSVError
      msg = "Malformed CSV"
      return false, msg
    end
  end

  #The following methods are called by the inject funnction (valid)
  def validate_first_name(passed_value)
    if(passed_value)
      return true
    else
      return false
    end
  end

  def validate_last_name(passed_value)
    if(passed_value)
      return true
    else
      return false
    end
  end

  def validate_email(passed_value)
    if(passed_value)
      return true
    else
      return false
    end
  end

  def validate_company(passed_value)
    if(passed_value)
      return true
    else
      return false
    end
  end

  def validate_roles(passed_value)
    if(passed_value)
      return true
    else
      return false
    end
  end
end
