## unescape and escape to CGI
module CoreExtensions
  ## unescape t
  def unescape
    CGI.unescape self
  end

  ## escape the string
  def escape
    CGI.escape self
  end

  ## encode the base64 string
  def encode
    Base64.encode self
  end

  ## decode the base64 string
  def decode
    Base64.decode self
  end

  ## Parsify the string to Hash
  def parse
    JSON.parse self
  end

  ## generate digest
  def digest
    OpenSSL::HMAC.digest('sha256',ApiConfig.privateKey, self)
  end
end
String.send(:include,CoreExtensions)
