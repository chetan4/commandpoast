# Load the rails application
require File.expand_path('../application', __FILE__)

require 'core_extensions'
require 'redis_connection'
require 'c_s_v_parser'
require 'string'
require 'magick_helper'
# Initialize the rails application
CommandPost::Application.initialize!
