CommandPost::Application.routes.draw do




  # The priority is based upon order of creation:
  # first created -> highest priority.
  ## add some route for vinod

  ### Please remove this after the UI is done
  
 
  root :to => 'users#login'
  

  match "/home" => "home#index",as: :home
  match "/facades/edit" => "facades#edit"

  get "home/index"

  scope "users" do
    get 'login' => 'users#login',as: :login
    post 'logged_in' => 'users#logged_in',as: :logged_in
    delete 'logout' => 'users#logout',as: :logout
  end

  resources :users,except: [:login,:logged_in,:logout] do
    collection do
      post 'upload'
    end
  end
  match '/sharetives/update_all' => 'sharetives#update_all', :via => :put
  resources  :sharetives
  
  resources :facades do
    match '/update/multiple' => 'facades#update_multiple', :on => :collection, :via => :put
    resources  :sharetives do
      match 'update/multiple' => 'sharetives#update_multiple', :on => :collection, :via => :put
    end
  end

  match '/sharetives/update_all' => 'sharetives#update_all', :via => :put

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
