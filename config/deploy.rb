set :application, 'CommandPoast'
set :repo_url, 'git@198.61.227.105:/home/git/commandpoast.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, 'master'
set :deploy_to, '/srv/apps/CommandPoast'
set :scm, :git

set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_files, %w{config/mongoid.yml config/api_config.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :rvm_type, :user
set :rvm_ruby_version, 'ruby-1.9.3-p484@commandpoast'
#set :default_env, { path: "/home/deploy/.rvm/bin/rvm/ruby-1.9.3-p484@commandpoast:$PATH" }
set :keep_releases, 5

set :bundle_gemfile, -> { release_path.join('Gemfile') }
set :bundle_dir, -> { shared_path.join('bundle') }
set :bundle_flags, '--deployment'
set :bundle_without, %w{development test}.join(' ')
#set :bundle_binstubs, -> { shared_path.join('bin') }
set :bundle_roles, :all
#set :default_shell, '/bin/bash -l'
#set :bundle_cmd, 'source $HOME/.bash_profile && bundle'

#set :migration_role, 'migrator' #defaults to db
set :normalize_asset_timestamps, %{public/images public/javascripts public/stylesheets}

after 'deploy:updated', 'deploy:precompile_assets'
namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
       execute :touch, release_path.join('tmp/restart.txt')
    end
  end



  task :precompile_assets do
    on roles(:app) do
      execute "cd /srv/apps/CommandPoast/current && /home/deploy/.rvm/bin/rvm ruby-1.9.3-p484@commandpoast do bundle exec rake assets:precompile RAILS_ENV=production"
    end
  end


  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'

end
