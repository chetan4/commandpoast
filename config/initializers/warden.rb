Rails.configuration.middleware.use RailsWarden::Manager do |manager|
  manager.default_strategies :api_authentication
  ## Write a failure app
  manager.failure_app = FailureApp #Proc.new { |env| ['200',{'Content-Type' => 'text/html'},['Logout You Idiot']]  }
end

Warden::Strategies.add(:api_authentication) do

  def valid?
     params[:user] and (params[:user][:email] || params[:user][:password])
  end

  def authenticate!
    u =  User.authenticate(data)
    u.nil? ? fail!(:invalid) : success!(u)
  end

  private
  def data
    result = if false
      nil
    else
      {user: params[:user]}
    end
    result
  end
end

# Setup Session Serialization
class Warden::SessionSerializer
  def serialize(record)
    [record.class.name, record.access_token]
  end

  def deserialize(keys)
    klass, token = keys
    klass.constantize.find(token)
  end
end

Warden::Manager.after_authentication do |user,auth,opts|
  ## Store the user fields in redis
  Store.put("users.#{user.access_token}",user.attributes.to_json)
end

Warden::Manager.before_logout do |user,auth,opts|
  ## Remove the user field from redis
  Store.del("users.#{user.access_token}")
end

Warden::Manager.after_set_user do |user,auth,opts|
  unless Store.find("users.#{user.access_token}")
    Store.put("users.#{user.access_token}",user.attributes.to_json)
  end
end

# Warden::Manager.after_failed_fetch do |user,auth,opts|
#   unless Store.find("users.#{user.access_token}").nil?
#     Store.del("users.#{user.access_token}")
#   end
# end
