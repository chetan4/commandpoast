
// takes facade object and generates markup for facade
function renderFacade(facade) {
    var facadeSTR = '<div class="facade" id="facadeId-' + facade.id +'" data-fid="' + facade.id +'" data-fpos="' + facade.position + '">';
        facadeSTR += '<div class="facadeHeader">';
        // facadeSTR += '<div class="tooltip">Double click on the title to edit</div>'; // Tooltip markup
        facadeSTR += '<div class="facadeControls">';
        facadeSTR += '<a href="#deleteFacadeModal" class="facadeDelete fontawesome-trash"></a>';
        facadeSTR += '<a href="#editfacade" class="facadeEdit fontawesome-pencil"></a>';
        facadeSTR += '<a class="facadeMove fontawesome-move"></a>';
        facadeSTR += '</div>';
        facadeSTR += '<h3>' + facade.name +'</h3>';
        facadeSTR += '</div>';
        facadeSTR += '<div class="facadeContents clearfix editFacadeConnection" id="facadeC-' + facade.id + '">';

        var facadeShareatives = findFacadeShareatives(facade.id);
        jQuery.each(facadeShareatives, function(index) {
            facadeSTR += renderShareative(facadeShareatives[index]);
        });

        facadeSTR += '</div>';
    facadeSTR += '</div>';

    return facadeSTR;
}

// sorts facades in the on-page model and renders them all in that order
function renderAllFacades() {
    var allStr = '';
    var x = facades.sort(function(a, b){
        if (a.position < b.position) return -1;
        if (a.position > b.position) return 1;
        return 0;})
    jQuery.each(x, function(index){
        allStr += renderFacade(x[index]);
    });
    return allStr;
}



// takes shareative object and generates markup
function renderShareative(shareative) {

    var shareativeSTR = '<div class="shareative" id="shareativeId-' + shareative.id +'" data-sid="' + shareative.id + '" data-spos="' + shareative.position + '">';
        shareativeSTR += '<div class="shareativeControls">';
            shareativeSTR += '<a href="#" class="shareativeEdit fontawesome-pencil" data-id="'+shareative.id+'"></a>';
            shareativeSTR += '<a class="shareativeMove fontawesome-move"></a>';
        shareativeSTR += '</div>';
        shareativeSTR += '<div class="shareativeAsset">';
        if (shareative.sharetive_type === "image") {
            shareativeSTR += '<div class="shareativeAssetImg"><img src="' + shareative.sharetive_url + '" alt="Thumb"></div>';          
        } else {
            // shareativeSTR += '<div class="videoShareative zocial-youtube"></div>';
            shareativeSTR += '<div class="videoShareative"><span class="youTubeIcon zocial-youtube"></span><img src="' + youtube_image(shareative.video_id) +'" alt="Thumb"></div>';            
        }
        shareativeSTR += '</div>';
    shareativeSTR += '<h4>' + shareative.name +'</h4>';
    shareativeSTR += '</div>';

    return shareativeSTR;
}

// uses on-page model and renders all shareatives
function renderAllShareatives() {
    var allStr = '';

    jQuery.each( shareatives, function(index){
        allStr += renderShareative(shareatives[index]);
    });
    return allStr;
}


// returns an ordered (by position) set of ACTIVE shareatives for the given facade
function findFacadeShareatives (facadeId) {
    return $.map(findALLFacadeShareatives(facadeId),
        function(item) {
            if (item.position != 0) return item;
        });
}

// returns an ordered (by position) set of INACTIVE shareatives for the given facade
function findInactiveFacadeShareatives (facadeId) {
    return $.map(findALLFacadeShareatives(facadeId),
        function(item) {
            if (item.position == 0) return item;
        });
}

// returns an ordered (by position) set of ALL sharetives for a given facade
function findALLFacadeShareatives (facadeId) {
    return $.map(shareatives,
        function(item){
            if (item.facadeId === facadeId) return item;}).sort(
            function(a, b){
                if (a.position < b.position) {
                    return -1;
                }
                if (a.position > b.position){
                    return 1;
                }
                return 0;
            });    
        }

/// MOVE FACADES

// gets the current order of facades via dom node order
function getCurrentFacadeOrder () {
    var x = [];
    var i = 1;
    $('.facade').each(function(){
       x.push({
            id : $(this).attr("data-fid"),
            // oldPosition : $(this).attr("data-fpos"),
            position : i++
        });
    });
    return x;
}


// after UI action, persist the position of the FACADES of SHAREATIVE to the backend
// and then update the on-page model


//// FACADE MOVEMENT ////

function postMoveFacade () {
    var currentOrder = getCurrentFacadeOrder();
    var obj = { facades : JSON.stringify(currentOrder)};

    console.log("POST MOVE FACADE")

    makeServerRequest('/facades/update/multiple.json', obj, "PUT", function (data) { // FIXME
        if (data.request_status === "success") {
            updateFacadeDataModel();
        }
    });
}

function updateFacadeDataModel () {
    console.log("Updating Facade Data-Model")
    var currentOrder = getCurrentFacadeOrder();

    for(i = 0; i < facades.length; i++) {
        $.map(currentOrder, function(item){
            if (item.id === facades[i].id) {
                facades[i].position = item.position;
            }
        });
    }
    console.log('Data-Model updated with current facade order');
}

//// SHARETIVE MOVEMENT ////

/// FACADE to FACADE SHARATIVES MOVEMENT
function makeShareativeUpdateRequest(obj, shareativePoolEditBool) {
    makeServerRequest('/sharetives/update_all.json', obj, "PUT", function (data) {
        console.log(data);
        if (data.request_status === "success") {
            updateShareativeDataModel();
            if (shareativePoolEditBool) {
                $('#shareativesContainerInner').find('.shareative').each(function(){
                    var sid = $(this).attr("data-sid");
                    $.map(shareatives, function(item) {
                        if (item.id === sid) {
                            item.position = 0;
                        }
                    })
                });   
            }
        }
    });
}

function getCurrentShareativeOrder () {
    var fid, x=[], i;
    $('.facade').each(function(){
        fid = $(this).attr("data-fid");
        i = 1;
        $(this).find('.shareative').each(function(){
            x.push({
                facadeId : fid,
                id : $(this).attr("data-sid"),
                position : i++
            });
        });
    });
    return x;
}

function postMoveShareative() {
    var currentOrder = getCurrentShareativeOrder();
    var obj = {shareatives : JSON.stringify(currentOrder)};

    console.log("POST MOVE SHARATIVES");
    console.log(obj);

    makeShareativeUpdateRequest(obj);
}

function updateShareativeDataModel () {
    console.log("Updating Sharetive Data-Model")
    var currentOrder = getCurrentShareativeOrder();

    for(i = 0; i < shareatives.length; i++) {
        $.map(currentOrder, function(item){
            if (item.id === shareatives[i].id) {
                shareatives[i].facadeId = item.facadeId;
                shareatives[i].position = item.position;
            }
        });
    }
    console.log('Data-Model updated with current Shareative order');
}

/// FACADE EDIT MODE - SHARETIVE MOVEMENT 

function getEditedFacadeShareativeOrder (facadeId) {
    var x=[], i=1;
    $("#facadeId-" + facadeId).find('.shareative').each(function(){
        x.push({
            facadeId : facadeId,
            id : $(this).attr("data-sid"),
            position : i++
        });
    });
    return x;
}

function updateEditFacadeShareativePool (facadeId) {
    var x=[];
    $('#shareativesContainerInner').find('.shareative').each(function() {
        x.push({
            facadeId : facadeId,
            id : $(this).attr("data-sid"),
            position : 0
        });
    });
    return x;
}

function postMoveFacadeEdit (facadeId) {
    var affectedShareatives = getEditedFacadeShareativeOrder(facadeId);
    affectedShareatives = affectedShareatives.concat(updateEditFacadeShareativePool(facadeId));

    var obj = {shareatives : JSON.stringify(affectedShareatives)};

    console.log("POST FACADE EDIT");
    console.log(obj);

    makeShareativeUpdateRequest(obj, true);
}

////////////////////////////////////////////////////////////////////

/// FACADES ADD / DELETE / EDIT
// ADD
function addNewFacade () {
    var obj;
    var facadeName = $('#addNewFacadeFormName').val();
    obj = { facade : 
            {
                name : facadeName,
                position: facades.length + 1
            }
        };
    makeServerRequest("/facades.json", obj, "POST", postNewFacade);
}

function postNewFacade (data) {
    if (data.request_status === 'success') {
        var newFacade = {
            company_id: data.data.company_id,
            id: data.data.id,
            name: data.data.name,
            position: data.data.position
        };
        facades.push(newFacade);
        var markup = renderFacade(data.data);
        $('#facadesContainerInner').append(markup);
        // reiniting the Jquery UI drag & drop thingie
        facadeMovementInit();
        $('#show-all-shareatives').show();
        console.log("Added FACADE Successuflly");
        console.log("FACADE Data-Model Updated");
    } else {
        console.log("Could NOT Add FACADE !!!!");
    }
}


// DELETE
// facades/facade_id.json with delete method

var zourfid = "";
function facadeDelete (fid, deleteShareativesBool) {
    url = "/facades/" + fid + ".json";
    zourfid = fid;

    var obj;
    if (deleteShareativesBool) {
        obj = {
            delete_shareatives : true
        }
    } else {
        obj = null;
    }

    console.log("initiating delete of facade " + fid + " delete_shareatives = " + deleteShareativesBool);
    makeServerRequest(url, obj, "DELETE", postFacadeDelete);
}

function postFacadeDelete (data) {
    if (data.request_status === 'success') {
        console.log(data);
        $("#" + "facadeId-" + zourfid).remove();
        console.log("DELETED FACADE Successuflly");
        removeFacadeShareatives(zourfid);
        removeDeletedFacade(zourfid);

        if (facades.length === 0) {
          $('#show-all-shareatives').hide();  
        }  
        // if (data.delete_shareatives == "true") {
        // }
        postMoveFacade();
    } else {
        console.log("Could NOT DELETE FACADE !!!!");
    }
}

function removeDeletedFacade(facadeId) {
    facades = _.reject(facades, function (item){
        if(item.id === facadeId) return true;
    });

}
function removeFacadeShareatives(facadeId) {
    shareatives = _.reject(shareatives, function (item){
        if(item.facadeId === 0) return true;
    });
    shareatives = _.reject(shareatives, function (item){
        if(item.facadeId === facadeId) return true;
    });

    console.log("Updated Shareatives data-model");
}

// backend
// transaction start
// update all sharetives set :facadeId="0" :where :facadeId = facade-id
// delete facade where :facadeId = facade-id
// end transaction

// EDIT

/// SHAREATIVES ADD / DELETE / EDIT
// ADD

function postNewShareative (data) {
    if (data.request_status === 'success') {
        var markup = renderShareative(data.data);
        $('#shareativesContainerInner').append(markup);
        console.log("Added Sharetive Successuflly");
    } else {
        console.log("Could NOT Add Sharetive !!!!");
    }
}

// DELETE
// facade/facade_id/sharetives/sharetive_id.json for saretive delete

function postShareativeDelete (data) {
    if (data.request_status === 'success') {
        console.log(data);
        shareatives = _.reject(shareatives, function (item){
            if(item.id === data.data[0].id){
                return true;  
            } 
        });
        hideModal($('#editShareativeModal'));
        $('#shareativesContainerInner').html(renderAllShareatives());
    }    
}

function deleteShareative (sid, fid) {
    var url = "/facades/" + fid + "/sharetives/" + sid + ".json"
    makeServerRequest(url, null, "DELETE", postShareativeDelete);
}

// EDIT

// Render Facade relevant shareatives on the left
function relevantFacadeShareativesPool (facadeId) {

    var relShareative = '';
    var x = findInactiveFacadeShareatives(facadeId);

    for (i = 0; i < x.length; i++) {
        relShareative += renderShareative(x[i]);
    }

    return relShareative;
}

// Providing the ID of a facade to edit
function facadeToEdit(facadeId, callback) {
    var thisFacade = $('.facade[data-fid="' + facadeId +'"]');
    $('.facade').hide();
    $('.facade[data-fid="' + facadeId +'"]').show();
    thisFacade.find('.facadeMove').hide();
    $('.facade').removeClass('currentFacade');
    thisFacade.addClass('currentFacade');
    $('#shareativesContainerInner').addClass('editFacadeConnection');
    if( callback && typeof callback === 'function' ) {
        callback();
    }
}

// Rendering a list of available facades to edit in the nav header buttons
function editFacadeHeader (facadeId) {
    var facadeListHeader = '<ul class="editFacadesList clearfix">';

    $.each(facades, function(index){
        facadeListHeader += '<li id="' + facades[index].id + '">';
        facadeListHeader +=     '<a href="' + facades[index].id + '">' + facades[index].name + '</a>';
        facadeListHeader += '</li>';
    });

    facadeListHeader += '</ul>';
    return facadeListHeader;
}

// Editing Facade heading
function editFacadeInit (node) {
    node.hide();
    var originalStr = node.text();
    var str = '<form id="updateFacadeNameForm" class="clearfix">';
        str += '<input type="text" class="facadeHeadingInput" value="' + originalStr + '">';
        str += '<input type="submit" value="Set" class="setHeadingSubmit">';
        str += '<a class="cancelEditFacadeName entypo-cancel"></a>';
        str += '</form>';
    $(str).insertAfter(node);
    return originalStr;
}


/// FACADES NAME UPDATE

function setFacadeHeading (node) {
    var fid = node.parents('.currentFacade').data('fid');
    var url = '/facades/' + fid + '.json';
    node.show();
    if ( node.siblings().children('.facadeHeadingInput') ) {
        var textToSet = node.siblings().children('.facadeHeadingInput').val();    
    }
    
    node.text(textToSet);
    
    var obj =  {
        id: fid,
        name: textToSet
    }

    node.siblings().children('.facadeHeadingInput').remove();
    node.siblings().children('.setHeadingSubmit').remove();
    node.siblings().children('.cancelEditFacadeName').remove();
    
    makeServerRequest(url, obj, "PUT", postFacadeHeadingChange);
}

function postFacadeHeadingChange (data) {
    if (data.request_status === 'success') {
        console.log("Facade's name udpated Successuflly");
        $.map(facades, function(item) {
            if (item.id == data.data.id) {
                item.name = data.data.name;
                $('.facadesHeader').html(editFacadeHeader(item.id)).show();
                highLightSelected(item.id);
            }
        });
    } else {
        console.log("Could NOT update FACADE's name !!!!");
    }
}

function youtube_image(video_id) {
  return "http://img.youtube.com/vi/"+video_id+"/2.jpg";
}


// MAking server request (XHR)
function makeServerRequest(action, data, method, callback) {
    $.ajax({
        url: action,
        method: method,
        data: data,
        dataType: "JSON"
    }).done(function( data ) {
        if (callback && (typeof callback === 'function')) {
            callback(data);
        }
    });
}

function makeServerRequesttest (action,data,method,callback) {
    var data = {request_status : "success"};
    callback(data);
}


// UI Functions
// Rendering Select option to choose facades
function selectListFacades () {
    var selectList = '';
    $.each(facades, function(index){
        selectList += '<option value="' + facades[index].id + '">' + facades[index].name + '</option>'
    });

    return selectList;
}

function resizeContainer(containerId, offset, width) {
    $(containerId).animate({
        left: offset,
        width: width
    }, 500);
}

function configResizeLink (containerId, config) {
    $('.resizeLink').removeClass('disabled');
    config === 'enable' // If enable is provided as an argument
        ? $(containerId).find('.resizeLink').removeClass('disabled') 
        : $(containerId).find('.resizeLink').addClass('disabled')
}


function hideModal (modal_id) {
    $("#lean_overlay").fadeOut(200);
    $(modal_id).css({ 'display' : 'none' });
}

// Update Title
function updateTitleofContainer (pageTitle) {
    $('.viewsContainer .pageTitle').text(pageTitle);
}

// Updating route when new sharetive has been created
function updateRoute () {
    var url = window.location.href;
    var arr = url.split('#');
    if ( arr[1] === 'shareatives' ) {
        console.log('condition satisfied');
        $('#show-all-shareatives').trigger('click');
        console.log('On Shareatives container');
    }
}


// Highlighting Selected facade nav item in the header button
function highLightSelected (facadeId) {
    $('.editFacadesList > li').removeClass('selected');
    $('.editFacadesList > li').each(function(){
        if($(this).attr('id') === facadeId) {
            $(this).addClass('selected');
        }
    });
}


function facadeMovementInit () {
    $("#facadesContainerInner").sortable({
        handle: ".facadeMove",
        cursor: "move",
        placeholder: "sortable-placeholder",
        start: function( event, ui ) {
            ui.item.addClass('movingFacade');
        },
        stop: function( event, ui ) {
            postMoveFacade();
            ui.item.removeClass('movingFacade');
        }
    }).disableSelection();

    $(".facadeContents").sortable({
        handle: ".shareativeMove",
        cursor: "move",
        connectWith: ".facadeContents",
        stop: function( event, ui ) {
            postMoveShareative();
        }
    }).disableSelection();
}


// Eof
