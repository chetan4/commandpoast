$(function(){
    $('.profileDetail').click(function(){
        $(this).addClass('open');
        $(this).find('.list').show();
    });

    $('.profileDetail').hover(
        function(){
            // Do nothing
        },
        function(){
            $(this).find('.list').hide();
            $(this).removeClass('open');
        }
    );

    $('.list').hover(function(){
        $(this).show();
    });
});
