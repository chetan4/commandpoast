module ApplicationHelper
  def separator
    " | "
  end

  def flash_message
    if current_user
      current_user.flash_message
    end
  end

  def active_css_class(active_class)
    if active_class == controller_classes[controller_name]
      return 'current'
    end         
  end

  def controller_classes
    @css_mapper ||= {
      "users#index" => "users",
      "users#new" => "users",
      "users#create" => "users",
      "users#edit" => "users",
      "users#update" => "users",
      "users#destroy" => "users",
      "facades#index" => "facades",
      "facades#create" => "facades",
      "facades#new" => "facades",
      "facades#edit" => "facades",
      "facades#update" => "facades",
      "facades#destroy" => "facades",
      "sharetives#index" => "facades",
      "sharetives#create" => "facades",
      "sharetives#new"  => "facades",
      "sharetives#edit" => "facades",
      "sharetives#destroy" => "facades"
    }
    return @css_mapper
  end

  def controller_name
    [controller.controller_name,controller.action_name].join("#") 
  end

  def alert_list(alert_type)
    "#{alert_type}List"
  end

  def alert_box(alert_type)
    "#{alert_type}Box"
  end
end