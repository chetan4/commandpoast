module ApiUtil
  module ParamHelpers
    INVALID_KEYS = [:action, :controller, :format, :utf8, 'utf-8']
    def sanitize(params)
      new_params = {}
      params.each do |k, v|
        new_params[k] = v if !INVALID_KEYS.include?(k.to_sym)
      end
      new_params
    end
  end
end
