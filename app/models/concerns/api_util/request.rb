module ApiUtil
  class Request
    include ApiUtil::ParamHelpers


    #################################################################
    #curl --data "digest=8h0cJuUBccCdI4h3CyYYpgLSRk6Xnvs%2FBTJksO%2BqBg%3D%0A&api_key=46748c41aae79&email=siddharth%2B2%40idyllic-software.com&password=abcabcabc" --request POST http://localhost:3001/api/v1/sessions.json
    ##################################################################
    attr_accessor :request, :api_key, :params, :request_digest
    def initialize(request, params)
      @request = request
      @params = sanitize(params)
      @api_key = params[:api_key]
      @params.delete :digest
    end

    def generate_encode_string
      ApiUtil::Digest.generate_digest(query_string, _private_key)
    end

    def self.valid?(request, params)
      api_request = ApiUtil::Request.new(request, params)
      encode_string = CGI.unescape(params[:encode])
      #validation_digest = api_request.generate_request_digest
      validation_digest = ApiUtil::Digest.generate_digest(encode_string, api_request.private_key)
      validation_digest == api_request.request_digest
    end
  end
end
