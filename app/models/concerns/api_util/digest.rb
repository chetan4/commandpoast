module ApiUtil
  module Digest
    def self.generate_digest(query_string, _private_key)
      Base64.encode64(OpenSSL::HMAC.digest('sha256', _private_key, query_string))
    end
  end


  module Code
    def self.encode(encode_string)
      Base64.encode64(encode_string)
    end

    def self.decode(encoded_string)
      Base64.decode64(encoded_string)
    end

    def self.decode_json(json_string)
      JSON.parse(json_string)
    end
  end
end
