class User < ServiceObject
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  cattr_accessor :secret_key,:current_user
  # validate :email,:password,:first_name,:last_name,presence: true,if: :required?
  attr_reader :attributes
  attr_accessor :email,:id,:password,:access_token,:first_name,:last_name,:company,:lat,:lng,:access_token_timestamp,:access_token_expires_at,:flash_message,:roles,:access_controls,:poaster_instances

  VALID_ROLES = ["curator", "networker"]

  def initialize(attributes={})
    @attributes = attributes || {}
    build_attribute
  end

  def persisted?
    false
  end

  def valid?
    true
  end

  def access_controls
    ## For current user the access controls super would always be nil
    ## and we have to retrieve it
    @access_controls || self.class.retrieve_access_controls
  end

  def poaster_instances
    @poaster_instances || self.class.retrieve_poaster_instances
  end

  def company_id
    self.company[:id] || self.company["id"] rescue nil
  end

  def user_roles
    roles.map { |r| r.capitalize }.join(",")
  end

  def has_roles?(role)
    roles.find { |r| r.to_sym == role}
  end

  def reset_attributes
    build_attribute
  end

  def company_name
    company[:name]
  end  

  def save
    response = self.class.create_info(users: [as_json],path: '/api/v1/registrations.json',access_token: current_user.access_token,method: :post)
    if response.success?
      self.flash_message = "Successfully added #{self.full_name}"
      return true
    elsif response.failed?
      self.flash_message = response.errors
      return false
    end
  end

  def update_attributes(params)
    response = self.class.update_info(user: params,path: "/api/v1/users/#{id}.json",access_token: current_user.access_token,method: :put)
    if response.success?
      @attributes = response.data['user']
      reset_attributes
      self.flash_message = "Successfully updated #{self.full_name}"
      return true
    elsif response.failed?
      @attributes = params
      build_attribute
      self.flash_message = response.errors.join(' ')
      return false
    end
  end


  def destroy
    response = self.class.remove_info(path: "/api/v1/users/#{id}",access_token: current_user.access_token,method: :delete)
    if response.success?
      self.flash_message = "Successfully deleted #{self.full_name}"
      return true
    elsif response.failed?
      self.flash_message = response.errors
      return false
    end
  end

  def full_name
    [first_name,last_name].join(' ')
  end

  def build_attribute
    attributes.each do |attribute,value|
      send("#{attribute}=",value)
    end
  end

  def clean_up_password
    self.password = nil
  end

  def as_json
    {
      first_name: first_name,
      last_name: last_name,
      email: email,
      roles: roles,
      company_id: company_id
    }
  end


  class << self

    def has_valid_roles?(users)
      flag = true
      wrong_users = []
      users.each do |u|
        unless u[:roles] and VALID_ROLES.include?(u[:roles].downcase.strip)
          wrong_users << u[:first_name]
          flag = false
        end
      end
      current_user.flash_message = "Invalid role for #{wrong_users.join(', ')}" if not (wrong_users.blank? or wrong_users.empty?) and not flag
      return flag
    end

    def remove_special_chars(users)
      users.each do |u|
        u.each{|k, v| u[k] = u[k].strip.gsub("'", '') if not u[k].nil?}
      end
      return users
    end

    def find_and_save(file)
      ## Read CSV file
      if users = parse_csv_file(file)
        users = remove_special_chars(users)
        bulk_insert(users) if has_valid_roles?(users)
      end
    end

    def bulk_insert(users)
      response = create_info(users: users,path: '/api/v1/registrations.json',access_token: current_user.access_token,method: :post)
      if response.success?
        current_user.flash_message = 'Succesfully Uploaded the CSV File'
      else
        current_user.flash_message = response.errors
      end
    end

    def all
      response = fetch_info(path: '/api/v1/users',access_token: current_user.access_token)
      if response.success?
        users = response.data['users']
        build_object(users)
      else
        current_user.flash_message = response.errors
        nil
      end
    end

    def find_by_id(id)
      #fetch_info({:id =>  id,:path => '/',access_token: current_user.access_token})
      response = fetch_info(:path => "/api/v1/users/#{id}/edit",access_token: current_user.access_token,plain: true)
      if response.success?
        user = response.data['user']
        build_object(user)
      else
        current_user.flash_message = response.errors
        nil
      end
    end

    def find_by_token(token)
      reset_session(token)
      response = fetch_info(:path => "/api/v1/users/me.json",access_token: token,plain: true)
      if response.success?
        user = response.data['user']
        build_object(user)
      elsif response.failed?
        nil
        ## Dont throw warden over here lest you would get a cyclic call  :)
        #throw(:warden, :scope => :user, :message => response.errors)
      end
    end

    def reset_session(token)
      Store.del("users.#{token}")
    end

    def find(token)
      ## the find method is implicitly used for user object sigin
      build_from_store("users.#{token}") || find_by_token(token)
    end

    def authenticate(data={})
      response = fetch_info data.merge(path: '/api/v1/sessions.json',method: :post)
      if response.success?
        users = response.data['user']
        build_object(users)
      else
        throw(:warden,:message => response.errors.join)
      end
    end

    def retrieve_access_controls
      response = fetch_info(path: "/api/v1/users/#{current_user.id}/access_controls.json",access_token: current_user.access_token,plain: true)
      if response.success?
        response.data['access_controls']
      elsif response.failed?
        nil
      end
    end

    def retrieve_poaster_instances
      response = fetch_info(path: "/api/v1/users/#{current_user.id}/poaster_instances.json",access_token: current_user.access_token,plain: true)
      if response.success?
        response.data['poaster_instances']
      elsif response.failed?
        ## return nil because controller would throw and error for nil 
        nil
      end
    end

    private
    def build_object(users)
      case users
      when Array
        Rails.logger.info "******* User is - Array *****"
        users.map { |user| build_object(user) }
      when Hash
        Rails.logger.info "******* User is - Hash *****"
        new(users)
      end
    end

    def build_from_store(token)
      user = if (attributes = Store.find(token))
        build_object(attributes)
      else
        nil
      end
      user
    end

    def parse_csv_file(file)
      csv = CSVParser.new(file)
      status, msg = csv.validate_rows
      if status
        csv.to_hash
      else
        current_user.flash_message = msg
        return false
      end
    end
  end
end


