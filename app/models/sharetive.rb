class Sharetive
  include Mongoid::Document
  include Mongoid::Timestamps
  field :position,type: Integer
  field :sharetive,type: String
  field :sharetive_type,type: String
  field :video_url,type: String
  field :name,type: String
  field :description,type: String
  field :company_id,type: Integer
  before_validation :determine_sharetive_type
  #validate :valid_video_id,:if => :valid_condition?
  belongs_to :facade
  scope :for_company,lambda { |company_id| where(company_id: company_id)}
  mount_uploader :sharetive,SharetiveUploader

  index({position: 1})
  index({sharetive_type:1})

  def as_json
    json_hash =  {
      position: self.position,
      sharetive_type: self.sharetive_type,
      name: self.name,
      facadeId: self.facade_id.to_s,
      id: self._id.to_s,
      video_id: self.video_id,
    }

    if image?
      json_hash[:sharetive_url] = self.sharetive.url
    else
      json_hash[:sharetive_url] = self.video_url
    end
    json_hash
  end


  def video_id
    video_url =~ /watch\?v=(.+)/ 
    $1
  end

  def image?
    sharetive_type == 'image'
  end

  def video?
    sharetive_type == 'video'
  end

  private
  def valid_video_id
    self.errors.add(:video_url,'Not a valid url') unless video_url =~ /watch\?v=(.+)/
  end

  def valid_condition?
    self.video_url.present? and self.video?
  end

  def determine_sharetive_type
    self.sharetive_type = 
      if self.video_url.present? 
        'video'
      elsif  self.sharetive.present?
        'image'
      end 
  end  
end
Sharetive.create_indexes
