class Facade
  include Mongoid::Document
  include Mongoid::Timestamps
  attr_accessible :name,:position,:company_id,:poaster_instance_id
  attr_accessor :purge
  field :name,type: String
  field :position, type: Integer
  field :company_id,type: Integer
  field :poaster_instance_id,type: Integer
  scope :for_company,lambda { |company_id| where(company_id: company_id)}
  has_many :sharetives

  index({company_id: 1})
  index({position: 1})

  attr_accessible :name, :position

  def as_json
    {
      id: self._id.to_s,
      name: self.name,
      position: self.position,
      company_id: self.company_id
    }
  end


  def destroy_or_delete
    ## write .to_boolean method 
    if purge == "true"
      sharetives.destroy_all
      destroy
    else
      sharetives.update_all(facade_id: 0, position: 0)
      delete
    end
  end  
end
Facade.create_indexes