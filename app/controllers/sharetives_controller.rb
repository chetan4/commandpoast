class SharetivesController < ApplicationController
  before_filter :load_facade, :except => [:update_all,:edit,:update]
  before_filter :load_sharetive, :only => [:edit,:destroy,:update,:show, :update_multiple]
  prepend_before_filter :require_user


  #http://localhost:3002/facades/52aad08c2559dfac24000009/sharetives.json
  ######################################################################
  # Response {
  #             "request_status":"success",
  #             "data":
  #                     [
  #                       {"position":1,"sharetive_type":"image",
  #                        "name":"Golf Course",
  #                        "facade_id":"52aad08c2559dfac24000009",
  #                         "sharetive_url":"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd0a2559df0770000006/Club_Corp.jpeg?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=TQU7nWkOvLi8tcPr4MhznELhUq4%3D&Expires=1386930513"
  #                        },
  #                        {"position":2,
  #                        "sharetive_type":"image",
  #                         "name":"Second",
  #                         "facade_id":"52aad08c2559dfac24000009",
  #                         "sharetive_url":"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd6d2559df0770000008/Screen_Shot_2013-10-01_at_1.12.16_AM.png?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=20BAnpYdKBlTW81iSRXF5wO87vw%3D&Expires=1386930513"
  #                         }
  #                     ]
  #          }
  ######################################################################
  def index
    respond_to do |format|
      @sharetives = @facade.sharetives.entries

      format.html {}
      format.json {
        response_hash = {
            request_status: :success,
            data: @sharetives.as_json
        }
        render :json => response_hash, :status => :ok
      }
    end
  end

  def show
  end

  def new
    @sharetive = @facade.sharetives.build
  end

  # POST http://localhost:3002/facades/52aad08c2559dfac24000009/sharetives.json
  ######################################################################
  #
  # "sharetive"=>
  #  {
  #     "name"=>"Sharetive - 11",
  #     "position"=>"4",
  #     "sharetive_type"=>"image",
  #     "video_url"=>"",
  #     "sharetive"=> <File Contents>
  #   }
  #
  #  Response
  #  {
  #    "request_status": "success",
  #    "data": {
  #              "position":2,
  #             "sharetive_type": "image",
  #             "name": "Second",
  #             "facade_id": "52aad08c2559dfac24000009",
  #             "sharetive_url": "https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd6d2559df0770000008/Screen_Shot_2013-10-01_at_1.12.16_AM.png?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=cEwuB7p5gtAWYcTixQkDx%2BPd8f4%3D&Expires=1387278229
  #
  #  }
  #
  ######################################################################

  def create
    @sharetive = @facade.sharetives.new(params[:sharetive])
    @sharetive.company_id = current_user.company_id
    respond_to do |format|
      if @sharetive.with({safe: true}).save

        format.html{
          flash[:alert] = 'Successfully created sharetives'
          redirect_to '/facades/#shareatives'
        }

        format.json {
          response_hash = {request_status: :success, data: @sharetive.as_json}
          render json: response_hash, status: :ok
        }
      else
        format.html {
          render 'new'
        }

        format.json {
          response_hash = {request_status: :failure,
                           data: nil,
                           errors: @sharetive.errors.full_messages}
          render json: response_hash, status: :ok
        }
      end
    end

    # Ajax code for sharetive create please add data-remote=true for ajax in modal form
    # @sharetive = @facade.sharetives.new(params[:sharetive])
    # @sharetive.company_id = current_user.company_id
    # respond_to do |format|
    #   format.js do 
    #     if @sharetive.with({safe: true}).save
    #       @success = true
    #       @response_hash = {request_status: :success, data: [@sharetive.as_json]}
    #     else
    #       @success = false
    #       @response_hash = {request_status: :failure,data: nil,errors: @sharetive.errors.full_messages}
    #     end
    #   end   
    # end 
  end

  def edit
    @facades = Facade.for_company(current_user.company_id)
    respond_to do |format|
      format.js
    end  
  end

  # PUT http://localhost:3002/facades/52aad08c2559dfac24000009/sharetives/52aadd6d2559df0770000008.json
  ######################################################################
  #
  # "sharetive"=>
  #               {
  #                 "position": 3,
  #                 "name": 'New Name'
  #               }
  #  Response
  #  {
  #    "request_status": "success",
  #    "data": {
  #              "position":3,
  #             "sharetive_type": "image",
  #             "name": "New Name",
  #             "facade_id": "52aad08c2559dfac24000009",
  #             "sharetive_url": "https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd6d2559df0770000008/Screen_Shot_2013-10-01_at_1.12.16_AM.png?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=cEwuB7p5gtAWYcTixQkDx%2BPd8f4%3D&Expires=1387278229
  #
  #  }
  #
  ######################################################################
  def update
    respond_to do |format|
      if @sharetive.with({safe: true}).update_attributes(params[:sharetive])

        format.html {
         flash[:alert] = 'Successfully updated sharetives'
         redirect_to facades_path
        }

        format.json {
          response_hash = {request_status: :success, data: [@sharetive.as_json]}
          render json: response_hash, status: :ok
        }
      else
        format.html {
          render 'edit'
        }

        format.json {
          response_hash = {request_status: :failure,
                           data: nil,
                           errors: @sharetive.errors.full_messages}
          render json: response_hash, status: :ok
        }
      end
    end
    ## Ajax code for sharetive please a data-remote true in  modal
    # respond_to do |format|
    #   format.js do 
    #     if @sharetive.with({safe: true}).update_attributes(params[:sharetive])
    #       @success = true
    #       @response_hash = {request_status: :success, data: [@sharetive.as_json]}
    #     else
    #       @success = false
    #       @response_hash = {request_status: :failure,data: nil,errors: @sharetive.errors.full_messages}
    #     end
    #   end   
    # end 
  end

  # PUT http://localhost:3002/facades/52aad08c2559dfac24000009/sharetives/update/multiple.json
  ######################################################################
  #
  # "sharetives"=>
  #               [{
  #                 "id": "52aadd6d2559df0770000008",
  #                 "position": 3,
  #                 "name": 'New Name'
  #               },
  #                {
  #                 "id": "52aadd0a2559df0770000006",
  #                 "position": 1,
  #                 "name": 'test 1'
  #                }]
  #  Response
  #  {
  #    "request_status": "success",
  #    "update_success_count": "2"
  #    "data": [
  #               {:position=>1,
  #                :sharetive_type=>"image",
  #                :name=>"Golf Course",
  #                :facade_id=>"52aad08c2559dfac24000009",
  #                :sharetive_url=>"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd0a2559df0770000006/Club_Corp.jpeg?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=4XVEAA4jArnXTPVBrVW3G4Ge%2BG8%3D&Expires=1387278864"
  #               },
  #               {
  #                 :position=>2,
  #                 :sharetive_type=>"image",
  #                 :name=>"Second",
  #                 :facade_id=>"52aad08c2559dfac24000009",
  #                 :sharetive_url=>"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd6d2559df0770000008/Screen_Shot_2013-10-01_at_1.12.16_AM.png?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=UxT20GeNi7SxGsF4aemgxgdqIWk%3D&Expires=1387278864"
  #               }
  #             ]
  #   }
  #
  ######################################################################


    # {
    #  "shareatives" => [
    #          {
    #            "id" => "3",
    #            "facadeId" => "2",
    #            "position" => "4"
    #          }
    #  ]
    # }

  def update_all
    @errors = []
    respond_to do |format|
      if valid_params? and store_shareatives and has_saved?  
        format.html{
          flash[:alert] = 'Successfully updates sharetives'
          redirect_to facades_path
        }

        format.json {
          response_hash = {
            request_status: :success,
            update_success_count: params[:shareatives].count
          }
          render json: response_hash, status: :ok
        }
      else
        format.html{
          flash[:alert] = 'Failed to update'
          redirect_to facades_path
        }

        format.json {
          response_hash = {
            request_status: :failure,
            errors: @errors
          }
          render json: response_hash, status: :ok
        }
      end  
    end
  end

  def update_multiple
    @sharetives = []

    params[:sharetives].each do |_sharetive|
      sharetive = @facade.for_company(current_user.company_id).
          where('sharetives._id' => _sharetive[:id] ).last

      next if sharetive.nil?
      sharetive.position = sharetive[:position].to_i
      @sharetives.push(sharetive) if sharetive.with({safe:true}).save
    end

    respond_to do |format|
      format.html{
        flash[:alert] = 'Successfully deleted sharetives'
        redirect_to facade_sharetives_path(@facade)
      }

      format.json {
        response_hash = {request_status: :success,
                        data: @sharetives.as_json,
                        update_success_count: @sharetives.size.to_i
                       }
        render json: response_hash, status: :ok
      }

    end
  end

  # DELETE http://localhost:3002/facades/52aad08c2559dfac24000009/sharetives/52aadd6d2559df0770000008.json
  ######################################################################
  #
  #  Response
  #  {
  #    "request_status": "success",  #
  #    "data":
  #               {:position=>1,
  #                :sharetive_type=>"image",
  #                :name=>"Golf Course",
  #                :facade_id=>"52aadd6d2559df0770000008",
  #                :sharetive_url=>"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd0a2559df0770000006/Club_Corp.jpeg?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=4XVEAA4jArnXTPVBrVW3G4Ge%2BG8%3D&Expires=1387278864"
  #               }
  #   }
  #
  #
  ######################################################################
  def destroy
    respond_to do |format|
      if @sharetive.with({safe: true}).destroy
        format.html{
          flash[:alert] = 'Successfully deleted sharetives'
          redirect_to facade_sharetives_path(@facade)
        }

        format.json {
          response_hash = {request_status: :success, data: [@sharetive.as_json]}
          render json: response_hash, status: :ok
        }
      else
        format.html {
          flash[:alert] = 'Sharetive could not be deleted'
          redirect_to facade_sharetives_path(@facade)
        }

        format.json {
          response_hash =  {request_status: :failure, data: [@sharetive.as_json]}
          render json: response_hash, status: :ok
        }
      end
    end
  end

  private

  def load_facade
    @facade = Facade.for_company(current_user.company_id).find(params[:facade_id])
  end

  def load_sharetive
    @sharetive = Sharetive.for_company(current_user.company_id).find(params[:id])
  end

  def valid_params?

    params[:shareatives] = JSON.parse(params[:shareatives])
    unless params[:shareatives] and params[:shareatives].is_a?(Array)
      @errors.push({errors: 'Invalid Params'})
      return false
    end
    return true
  end
   
  def store_shareatives
    @errors = params[:shareatives].map do |shareative|
      update_shareative(shareative)
    end.compact
  end

  def update_shareative(shareative_attribute)
    shareative = Sharetive.find(shareative_attribute.delete(:id)) rescue nil
    return {:errors => 'No Sharetives found'} unless shareative
    facade_id = shareative_attribute.delete(:facadeId) 
    facade = Facade.for_company(current_user.company_id).find(facade_id) rescue nil
    return {:errors => 'No Facade Found'} unless facade
    shareative.facade_id = facade_id
    position =  shareative_attribute.delete(:position)
    return {:errors => 'No Position found'} unless position
    shareative.position = position.to_i
    return shareative.errors unless shareative.with({safe: true}).save
    nil
  end

  def has_saved?
    @errors.size.zero?
  end 
end
