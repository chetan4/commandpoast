class HomeController < ApplicationController
  before_filter :require_user
  def index
  end

  def home
  end
end
