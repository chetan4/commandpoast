class UsersController < ApplicationController
  before_filter :load_users,:only => [:index,:create,:new,:edit,:update]
  skip_before_filter :set_current_user,:only => [:login,:logged_in]
  prepend_before_filter :require_no_user ,:only => [:login,:logged_in]
  prepend_before_filter :require_user,:only => [:logout,:index,:new,:create,:edit,:update,:destroy]


  def index
    page = params[:page] || 1
    @users = Kaminari.paginate_array(@users).page(page).per(10)
  end

  def new
    @user = User.new
    @user.roles = [:curator]
  end

  def create
    @user = User.new(params[:user])
    @user.company = current_user.company
    if @user.valid? and @user.save
      flash[:alert] = @user.flash_message
      render 'new'
    else
      flash[:alert] = @user.flash_message.pop[:error].join(" ") if @user.flash_message.is_a?(Array)
      render 'new'
    end
  end

  def upload
    # if params[:users].present? and User.upload_csv
    if remotipart_submitted? and params[:user][:attachment]
      if User.find_and_save(params[:user][:attachment].tempfile)
        @msg = current_user.flash_message
        current_user.flash_message = nil
      else
        @msg = current_user.flash_message
      end
    else
      @msg = "Please select appropriate file."
    end

    respond_to do |format|
      format.js
    end
    ## Need to tackle all edge cases of file corrupted and other stuff
  end

  def edit
    @user = User.find_by_id(params[:id])
    if @user.nil? or current_user.flash_message.present?
      redirect_to home_path
      flash[:alert] = current_user.flash_message
    else
      render 'edit'
    end
  end

  def update
    @user = User.find_by_id(params[:id])
    if @user and @user.update_attributes(params[:user])
      flash[:alert] = @user.flash_message
      render 'edit'
    else
      flash[:alert] = @user.flash_message
      render 'edit'
    end
  end

  def destroy
    @user = User.find_by_id(params[:id])
    if @user and @user.destroy
      flash[:alert] = @user.flash_message
      redirect_to users_path
    else
      flash[:alert] = @user.flash_message
    end
  end

  def login
    build_user
    render layout: 'site'
  end


  def logged_in
    authenticate!(auth_options)
    if logged_in?
      flash[:alert] = 'Sucessfully Loggedin'
      redirect_to  (stored_path || home_path)
    else
      build_user
      flash[:alert] = @user.flash_message
      render 'login',layout: 'site'
    end
  end

  def logout
    super
    flash[:alert] = 'Logout Successfully'
    redirect_to login_path
  end

  def access_controls
    current_user.access_controls
  end

  private

  def build_user
    @user = User.new(params[:user] || {})
    clean_up_password
  end

  def clean_up_password
    @user.clean_up_password
  end

  def auth_options
    {recall: "#{controller_path}#login" ,message: "Invalid Email or Password" }
  end

  def load_users
    @users = User.all || []
  end
end
