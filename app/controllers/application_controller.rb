## Require this in application controller since the httparty lazy load
class ApplicationController < ActionController::Base
  protect_from_forgery
  # before_filter :authenticate_user!
  before_filter :set_current_user
  rescue_from Exception, with: :render_internal_server_error


  private
  def require_user
    unless current_user
      flash[:alert] = 'You must be logged in to access that page'
      store_location!
      redirect_to login_path
      return false
    end
  end

  def require_no_user
    if current_user
      flash[:alert] = 'You must be logged out to access that page'
      redirect_to home_path #|| request.referrer
      return false
    end
  end

  def store_location!
    session[:return_to] = return_to_path if request.get?
  end

  def stored_path
    session.delete(:return_to)
  end

  def return_to_path
    request.path_info
  end

  def set_current_user
    if current_user
      ## use current thread mechanism
      #Thread[:current]
      ## Need this to pass the token in the
      User.current_user = current_user
    end
  end

  def render_internal_server_error
    if request.format.json?
      render :json => {request_status: :failure }, :status => :internal_server_error
    else 
      raise
    end
  end
end
