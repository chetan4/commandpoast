class FacadesController < ApplicationController
  before_filter :check_authorization, :only => [:edit,:update,:destroy,:show]
  before_filter :load_facade ,:only => [:edit,:update,:destroy,:show]
  prepend_before_filter :require_user

  # GET http://localhost:3002/facades.json
  ################################################################################
  #curl -X GET http://localhost:3002/facades.json
  #{"facades":[
  #             {"id":"52aacbd02559dfac24000008","name":"Facade2","position":3}
  #           ],
  # "sharetives": [
  #               {:position=>1,
  #                :sharetive_type=>"image",
  #                :name=>"Golf Course",
  #                :facade_id=>"52aad08c2559dfac24000009",
  #                :sharetive_url=>"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd0a2559df0770000006/Club_Corp.jpeg?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=4XVEAA4jArnXTPVBrVW3G4Ge%2BG8%3D&Expires=1387278864"
  #               },
  #               {
  #                 :position=>2,
  #                 :sharetive_type=>"image",
  #                 :name=>"Second",
  #                 :facade_id=>"52aad08c2559dfac24000009",
  #                 :sharetive_url=>"https://poaster.s3.amazonaws.com/uploads/sharetive/sharetive/52aadd6d2559df0770000008/Screen_Shot_2013-10-01_at_1.12.16_AM.png?AWSAccessKeyId=AKIAJQWK7V4DKTHYWXDA&Signature=UxT20GeNi7SxGsF4aemgxgdqIWk%3D&Expires=1387278864"
  #               }
  #             ]
  #  }
  ################################################################################

  def index
    @facades = Facade.for_company(current_user.company_id).order_by([:position, :asc])
    facade_ids = @facades.collect(&:id)
    @facade  = Facade.new(company_id: current_user.company_id)
    @sharetives = Sharetive.for_company(current_user.company_id).order_by([:position, :asc])

    respond_to do |format|
      format.html {}
      format.json {
        response_hash = {}
        response_hash[:facades] = @facades.as_json
        response_hash[:sharetives] = @sharetives.as_json
        render :json => response_hash, :status => :ok
      }
    end
  end

  def show
    respond_to do |format|
      format.html {}
      format.json {
        render json: { request_status: :success, data: @facade.as_json }
      }
    end
  end

  def new
    @facade = Facade.new(company_id: current_user.company_id,poaster_instance_id: first_poaster_instance)
  end

  # POST http://localhost:3002/facades.json
  ################################################################################
  #curl -X POST --header "Content-Type:application/json" -d '{"facade":
  #                                                             { "name": "Facade2",
  #                                                               "position": 2}
  #                                                             }'
  #                       http://localhost:3002/facades.json
  # Response:
  # {"request_status":"success",
  # "data":{"id":"52aac9da2559dfac24000006","name":"Facade2","position":2}}
  ################################################################################
  def create
    @facade = Facade.new(params[:facade])
    @facade.company_id = current_user.company_id
    @facade.poaster_instance_id = first_poaster_instance
    respond_to do |format|
      if @facade.with({safe: true}).save
        format.html {
          flash[:alert] = 'Successfully created facades'
          redirect_to '/facades'
        }

        format.json {
          response_hash = {request_status: :success,
                           data: @facade.as_json
                          }

          render :json => response_hash, :status => :ok
        }
      else
        format.html{ render 'new' }
        format.json {
          response_hash = {
              request_status: :failure,
              errors: @facade.errors.full_messages,
              data: nil
          }
          render :json => response_hash, :status => :ok
        }
      end
    end
  end

  def edit
    @facades = Facade.for_company(company_id: current_user.company_id)
    @facade = Facade.find(params[:id])
  end

  # PUT http://localhost:3002/facades/update/multiple
  #########################################################################
  #  curl -X PUT --header "Content-Type:application/json"
  # -d '{"facades":
  #                [{ "name": "Facade2", "position": 2, "id":"52aacbd02559dfac24000008"},
  #                 {"id":"52aad08c2559dfac24000009", "position":2, "name": "Facade1"}
  #                ]}'
  #
  #
  # Response
  # "request_status":"success",
  # "data":
  #         [
  #           {"id":"52aacbd02559dfac24000008",
  #            "name":"Facade2",
  #            "position":2,"company_id":1
  #           },
  #           {"id":"52aad08c2559dfac24000009",
  #           "name":"Facade1",
  #           "position":2,
  #            "company_id":1
  #           }
  #        ],"update_success_count":2}
  #
  ##########################################################################
  def update_multiple
     @facades = []
     if params[:facades].present?
      params[:facades] = JSON.parse(params[:facades])
      params[:facades].each do |_facade|
         facade = Facade.for_company(current_user.company_id).
                         where(:_id => _facade[:id]).last

         next if facade.nil?
         facade.position = _facade[:position].to_i
         @facades << facade if facade.with({safe:true}).save
       end
     end

     respond_to do |format|
       format.html {
         redirect_to facades_path
       }

       format.json {
         response_hash = {request_status: :success,
                          data: @facades.as_json,
                          update_success_count: @facades.size.to_i
                         }

         render :json => response_hash, :status => :ok
       }
     end
  end

  # PUT http://localhost:3002/facades/52aacbd02559dfac24000008.json
  ################################################################################
  #curl -X PUT --header "Content-Type:application/json" -d
  #                     '{"facade": { "name": "Facade2",
  #                                   "position": 3}}'
  #                   http://localhost:3002/facades/52aacbd02559dfac24000008.json
  #
  #
  # Response {"request_status":"success",
  #            "data":{"id":"52aacbd02559dfac24000008","name":"Facade2","position":3}}
  ################################################################################
  def update
    respond_to do |format|
      if @facade.with({safe: true}).update_attributes(name: params[:name])

        format.html{
          flash[:alert] = 'Successfully update facade'
          redirect_to facades_path
        }

        format.json{
          response_hash = {request_status: :success, data: @facade.as_json}
          render :json => response_hash, :status => :ok
        }
      else
        format.html{
          render 'edit'
        }

        format.json {
          response_hash = {request_status: :failure, data: nil}
          render :json => response_hash, status: :ok
        }
      end
    end
  end


  def destroy
    @facade.purge = params[:delete_shareatives] || params["delete-shareatives"]
    respond_to do |format|
      if @facade.destroy_or_delete
        format.html {
          flash[:alert] = 'Successfully Deleted facade'
          redirect_to facades_path
        }

        format.json {
          render :json => {request_status: :success, data: @facade.as_json, delete_shareatives: :delete_shareatives},status: :ok
        }
      else
        format.html{
          flash[:alert] = 'Facade could not be deleted'
          redirect_to facades_path
        }

        format.json {
          render :json => {request_status: :failure, data: @facade.as_json}
        }
      end
    end
  end


  private
  def load_facade
    @facade = Facade.for_company(current_user.company_id).find(params[:id])
  end

  def check_authorization
    redirect_to home_path unless authorize_user
  end

  def authorize_user
    current_user.has_roles?(:super_admin) || current_user.has_roles?(:poast_master)
  end

  def first_poaster_instance
    current_user.poaster_instances.first['id']
  end
  ################################################################################################
  # Uncomment to Test with CURL
  # def current_user
  #  User.new(Store.find('users.60503b0c1a9b55e297ed56c3752b7defe83b0491b53995d38629ade117f84764'))
  # end
  ################################################################################################
end
